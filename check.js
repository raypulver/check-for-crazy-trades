'use strict';

const {promisify} = require('bluebird');
const {flow, omitBy} = require('lodash/fp');

const {argv: {_: [from]}} = require('yargs');

const {mapValues, bindKey, property} = require('lodash');
const fetch = promisify(require('request'));

const apiCall = route => payload =>
  fetch({
    url: require('./config').api + '/' + route,
    method: 'POST',
    json: payload,
  }).then(property('body')).then((v) => {
    try {
      return JSON.parse(v);
    } catch (e) { return v; }
  });

const {returnTradeHistory, returnTicker} = [
  'returnTradeHistory',
  'returnTicker',
].reduce((r, v) => ((r[v] = apiCall(v)), r), {});

const log = bindKey(console, 'log');
const util = require('util');
const ln = v => (log(util.inspect(v, {colors: true, depth: 100})), v);

Promise.all([
  returnTicker(),
  returnTradeHistory({
    start: +from,
    end: Math.floor(Date.now() / 1000),
  }),
])
  .then(([tickers, history]) =>
    mapValues(history, (v, market) =>
      v.filter(v => {
        let lastPrice = (tickers[market] || {}).last;
        if (!lastPrice || lastPrice === 'N/A') lastPrice = v.price;
        lastPrice = +lastPrice;
        const diff = Math.abs(+v.price - lastPrice)/lastPrice;
        return diff > 4;
      }),
    ),
  )
  .then(omitBy((v) => !v.length))
  .then(ln)
  .catch(flow(property('stack'), console.error));
